# Composer Patches Reporter

## Prototype roadmap

- [x] Implement a Composer plugin
- [x] Implement a Composer command `list-patches`
- [x] Output the list of patches from `cweagans/composer-patches`

