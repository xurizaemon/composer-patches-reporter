<?php

namespace xurizaemon\Composer\PatchesReporter;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\Command\BaseCommand;

class CommandListPatches extends BaseCommand
{
    protected function configure(): void
    {
        $this->setName('list-patches');
        $this->setDescription('List patches used in this project.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Feels like a rough way to find cweagans/composer-patches.
        $plugins = $this->getComposer()->getPluginManager()->getPlugins();
        foreach ($plugins as $plugin) {
          if (get_class($plugin) === 'cweagans\Composer\Patches') {
            $patchesData = $plugin->grabPatches();
            foreach ($patchesData as $component => $patches) {
              foreach ($patches as $description => $url) {
                $rows[] = [
                  $component,
                  $description,
                  $url,
                ];
              }
            }
          }
        }
        // Write $rows as CSV, etc.
        return 0;
    }
}
