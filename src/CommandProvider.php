<?php

namespace xurizaemon\Composer\PatchesReporter;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use xurizaemon\Composer\PatchesReporter\CommandListPatches;

class CommandProvider implements CommandProviderCapability
{
    public function getCommands()
    {
        return array(new CommandListPatches);
    }
}
