# Notes

## Commands not showing up until the plugin package is required into a project

It appears that a Composer plugin's commands are not loaded if you're working in the context of the Composer package that provides the commands. I was hopeful that I'd be able to work on the package in its own repository, but the commands only appeared after I required this package into another project.

Should look into how other plugin packages test and develop.

